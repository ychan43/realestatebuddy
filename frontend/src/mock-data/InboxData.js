const InboxData = {
    "message": "OK",
    "data": [
        {
            "_id": "55099652e5993a350458b7b7",
            "dateCreated": "2016-05-18T16:00:00Z",
            "sender": "test1@gmail.com",
            "recipient": "a@b.com",
            "property_id": "55099652e5993a350458b7b7",
            "property_address": "123 Green St",
            "apt_number": "100A",
            message: 'this is a test message yes',
        },
        {
            "_id": "55099652e5993a350458b7b7",
            "dateCreated": "2016-05-18T16:00:00Z",
            "sender": "test2@gmail.com",
            "recipient": "a@b.com",
            "property_id": "55099652e5993a350458b7b7",
            "property_address": "123 Green St",
            "apt_number": "100A",
            message: 'this is a test message 67',
        },
        {
            "_id": "55099652e5993a350458b7b7",
            "dateCreated": "2016-05-18T16:00:00Z",
            "sender": "test3@gmail.com",
            "recipient": "a@b.com",
            "property_id": "55099652e5993a350458b7b7",
            "property_address": "123 Green St",
            "apt_number": "100A",
            message: 'this is a test message first',
        },
    ]
}

export default InboxData;