import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import PostParams from '../components/PostParams';

function CreatePost({ user }) {
  const [price, setPrice] = useState('');
  const [beds, setBeds] = useState('');
  const [baths, setBaths] = useState('');
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [aptNumber, setAptNumber] = useState('');
  const [city, setCity] = useState('');
  const [state, setState] = useState('');
  const [zipcode, setZipcode] = useState('');
  const [sqft, setSqft] = useState('');
  const [desc, setDesc] = useState('');

  const [amenities, setAmenities] = useState({
    pet: false,
    wifi: false,
    ac: false,
    laundry: false,
    dishwasher: false,
    furnished: false,
    parking: false,
    balcony: false,
    gym: false,
    pool: false,
  });

  return (
    <Container>
      <PostParams
        name={name}
        setName={setName}
        address={address}
        setAddress={setAddress}
        aptNumber={aptNumber}
        setAptNumber={setAptNumber}
        city={city}
        setCity={setCity}
        state={state}
        setState={setState}
        zipcode={zipcode}
        setZipcode={setZipcode}
        sqft={sqft}
        setSqft={setSqft}
        price={price}
        setPrice={setPrice}
        beds={beds}
        setBeds={setBeds}
        baths={baths}
        setBaths={setBaths}
        amenities={amenities}
        setAmenities={setAmenities}
        desc={desc}
        setDesc={setDesc}
        user={user}
      />
    </Container>
  );
}

export default CreatePost;
