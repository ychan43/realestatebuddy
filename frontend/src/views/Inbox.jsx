import React, { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import axios from 'axios';
import Message from '../components/Message';

function Inbox({ user }) {
  const [messageData, setMessageData] = useState([]);

  useEffect(() => {
    axios.get('https://thawing-plateau-85192.herokuapp.com/api/messages?where={"recipient":"' + user.email + '"}').then((res) => {
      setMessageData(res.data.data);
    }).catch(err => console.log(err));
  }, [user])

  return (
    <Container>
      <h1 className='text-center mt-5'>
        Inbox
      </h1>
      <div className="mt-5">
        {messageData.length === 0 && <h4 className='text-center mt-5 text-muted'>
          Your Inbox is Empty
        </h4>}
        {messageData.map((msg) => <Message message={msg} />)}
      </div>
    </Container>
  );
}

export default Inbox;
