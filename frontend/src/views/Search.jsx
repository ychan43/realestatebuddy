import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import SearchParams from '../components/SearchParams';
import ListView from '../components/ListView';

function Search({ results, setResults }) {
  const [location, setLocation] = useState('');
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [beds, setBeds] = useState('');
  const [baths, setBaths] = useState('');
  const [sqft, setSqft] = useState('');

  const [amenities, setAmenities] = useState({
    pet: false,
    wifi: false,
    ac: false,
    laundry: false,
    dishwasher: false,
    furnished: false,
    parking: false,
    balcony: false,
    gym: false,
    pool: false,
  });

  const [locType, setLocType] = useState(0);

  return (
    <Container className='mb-5'>
      <SearchParams
        locType={locType}
        setLocType={setLocType}
        location={location}
        setLocation={setLocation}
        minPrice={minPrice}
        setMinPrice={setMinPrice}
        maxPrice={maxPrice}
        setMaxPrice={setMaxPrice}
        beds={beds}
        setBeds={setBeds}
        baths={baths}
        setBaths={setBaths}
        sqft={sqft}
        setSqft={setSqft}
        amenities={amenities}
        setAmenities={setAmenities}
        setResults={setResults}
        />
      <ListView results={results}/>
    </Container>
  );
}

export default Search;
