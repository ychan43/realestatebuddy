import React, { useState } from 'react';
import Firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import { Card, Button, Form } from 'react-bootstrap';
import { useHistory } from "react-router-dom";
import axios from 'axios';

// Google account created for Firebase
//
// email: cs498rkfinal@gmail.com
// password: finalproject

// Log in at the link below with the above credentials to see users
// https://console.firebase.google.com/u/6/project/real-estate-buddy/authentication/users

function Login({ user, setUser }) {
  const Auth = Firebase.auth();
  const [email, setEmail] = useState('');
  const [pass1, setPass1] = useState('');
  const [pass2, setPass2] = useState('');
  const [loginEmail, setLoginEmail] = useState('');
  const [loginPass, setLoginPass] = useState('');
  const history = useHistory()

  function signUp() {
    if (pass1 !== pass2) {
      alert("Passwords do not match");
      return;
    }

    Auth.createUserWithEmailAndPassword(email, pass1)
      .then((userCredential) => {
        setUser(userCredential.user);
        history.push("/")
      })
      .catch(e => alert(e.message));

    const user = {
      email: email,
    };

    axios.post('https://thawing-plateau-85192.herokuapp.com/api/users',
      user
    ).catch(err => (console.log(err)));
  }

  function login() {
    Auth.signInWithEmailAndPassword(loginEmail, loginPass)
      .then((userCredential) => {
        setUser(userCredential.user)
        history.push("/")
      })
      .catch(e => alert(e.message));
  }

  return (
    <>
    <div className="d-flex justify-content-around container mt-5">
      <Card style={{ width: '18rem' }}>
        <Card.Body>
          <Card.Title>Sign Up Now!</Card.Title>
          <Form>
            <Form.Group controlId='email'>
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.currentTarget.value)}
                />
            </Form.Group>

            <Form.Group controlId="pass1">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={pass1}
                onChange={(e) => setPass1(e.currentTarget.value)}
                />
            </Form.Group>
            <Form.Group className="mb-3" controlId="pass2">
              <Form.Label>Repeat Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={pass2}
                onChange={(e) => setPass2(e.currentTarget.value)}
                />
            </Form.Group>
            <Button variant="primary" onClick={signUp}>
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>

      <Card style={{ width: '18rem' }}>
        <Card.Body>
          <Card.Title className='mb-4'>Login</Card.Title>
          <Form>
            <Form.Group controlId='loginEmail' className='pt-2'>
              <Form.Label>Email Address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={loginEmail}
                onChange={(e) => setLoginEmail(e.currentTarget.value)}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="loginPass">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={loginPass}
                onChange={(e) => setLoginPass(e.currentTarget.value)}
                />
            </Form.Group>
            <Button variant="primary" onClick={login}>
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </div>
    </>
  );
}

export default Login;
