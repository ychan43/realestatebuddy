import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Container, Badge } from 'react-bootstrap';
import axios from 'axios';
import PropertyDetail from '../mock-data/PropertyDetail';
import DetailNavigation from '../components/DetailNavigation';

function Detail({ user }) {
  const { buildingID } = useParams();
  const [data, setData] = useState({});
  const ammenitiesMap = {
      pet: 'Pet Friendly',
      wifi: 'Wi-Fi',
      ac: 'Air Conditioning',
      laundry: 'In-Unit Washer/Dryer',
      dishwasher: 'Dishwasher',
      furnished: 'Furnished',
      parking: 'Parking Available',
      balcony: 'Balcony',
      gym: 'Gym',
      pool: 'Pool',
  }

  useEffect(() => {
    axios.get('https://thawing-plateau-85192.herokuapp.com/api/properties/' + buildingID).then((res) => {
      setData(res.data.data);
    }).catch(err => console.log(err));
  }, [buildingID])

  return (
    <Container className='my-5'>
      <h1>
        {data.name} <Badge bg="primary">${data.price}</Badge>
      </h1>
      <h3 className='text-muted'>
        {data.address} {data.apt_number}, {data.city}, {data.state} | Beds: {data.bedrooms} | Baths: {data.bathrooms} | Square Feet: {data.squarefeet} | Zip Code: {data.zipcode}
      </h3>
      <p>
        {data.description}
      </p>
      <p>
        Amenities: {data.amenities ? Object.entries(data.amenities).map(([key, val]) =>
          val && <>
            <Badge bg="secondary">{ammenitiesMap[key]}</Badge>&nbsp;
          </>
        ) : 'N/A'}
      </p>
      <DetailNavigation user={user} data={data}/>
    </Container>
  );
}

export default Detail;
