import './App.css';
import React, { useState } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './views/Login';
import Search from './views/Search';
import CreatePost from './views/CreatePost';
import Detail from './views/Detail';
import Inbox from './views/Inbox';
import Navigation from './components/Navigation';

function App() {
  const [user, setUser] = useState({})
  const [results, setResults] = useState({});

  return (
    <div className="App">
        <Router>
          <Switch>
            <Route path="/login">
              <Navigation user={user} setUser={setUser}/>
              <Login user={user} setUser={setUser}/>
            </Route>
            <Route path="/post">
              <Navigation user={user} setUser={setUser}/>
              <CreatePost user={user}/>
            </Route>
            <Route path="/details/:buildingID" component={Detail}>
              <Navigation user={user} setUser={setUser}/>
              <Detail user={user}/>
            </Route>
            <Route path="/inbox">
              <Navigation user={user} setUser={setUser}/>
              <Inbox user={user}/>
            </Route>
            <Route path="/">
              <Navigation user={user} setUser={setUser}/>
              <Search results={results} setResults={setResults}/>
            </Route>
          </Switch>
        </Router>
    </div>
  );
}

export default App;
