import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  BrowserRouter as Router
} from "react-router-dom";
import Firebase from "firebase/compat/app";

Firebase.initializeApp({
  apiKey: "AIzaSyCBKoafc6fRZGelydPPh8w0ttSBVsKyCvo",
  authDomain: "real-estate-buddy.firebaseapp.com",
  projectId: "real-estate-buddy",
  storageBucket: "real-estate-buddy.appspot.com",
  messagingSenderId: "150523908886",
  appId: "1:150523908886:web:8b2cdafe598f4077f23637"
});

ReactDOM.render(
  <Router basename={process.env.PUBLIC_URL}>
    <App />
  </Router>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
