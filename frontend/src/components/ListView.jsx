import React from 'react';
import Listing from './Listing';

const ListView = ({ results }) => {
    return (
        <div className='mt-4'>
            {results?.map?.((property) => 
                <Listing data={property} />
            )}
        </div>
    );
};

export default ListView;
