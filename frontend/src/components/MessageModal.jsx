import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';
import axios from 'axios';

function MessageModal({ data, show, onHide, user }) {
    const [message, setMessage] = useState('');
    const sendMessage = () => {
        if (message === '') {
            alert('Please input a message before sending.');
            return;
        }
        const req = {
            message,
            sender: user.email,
            recipient: data.creator,
            property_id: data._id,
            property_address: data.address,
            apt_number: data.apt_number,
        }
        console.log(req);

        axios.post('https://thawing-plateau-85192.herokuapp.com/api/messages',
          req
        ).catch(err => (console.log(err)));

        setMessage('');
        onHide();
    }

    return (
      <Modal
        show={show}
        onHide={onHide}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            {data.name}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>Contact Property Owner</h4>
            <Form.Group className="mb-2">
                <Form.Label>
                    By sending a message, the recipient will have access to your email address
                    . Please note that if the property owner responds, it will be via email correspondence.
                </Form.Label>
                <Form.Control
                    as="textarea"
                    rows={5}
                    value={message}
                    onChange={(e) => setMessage(e.currentTarget.value)}
                />
            </Form.Group>
        </Modal.Body>
        <Modal.Footer className="d-flex justify-content-between">
            <Button onClick={onHide} variant='danger'>Close</Button>
            <Button onClick={sendMessage}>Send!</Button>
        </Modal.Footer>
      </Modal>
    );
  }

export default MessageModal
