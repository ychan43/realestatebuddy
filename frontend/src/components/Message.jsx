import React from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Message = ({ message }) => {
    return (
        <Card>
            <Card.Body>
                <div className="d-flex justify-content-between">
                    <div>
                        <Card.Title>Inquiry about&nbsp;
                            <Link to={`/details/${message.property_id}`}>
                                {message.property_address} {message.apt_number}
                            </Link> from {message.sender}
                        </Card.Title>
                    </div>
                    <div>
                        <Card.Subtitle>{message.dateCreated}</Card.Subtitle>
                    </div>
                </div>
                <Card.Text>
                    {message.message}
                </Card.Text>
                <div className="d-flex justify-content-between">
                    <Card.Subtitle className="mb-2 text-muted">
                        To reply to this inquiry, please email the sender directly.
                    </Card.Subtitle>
                    <a href={`mailto:${message.sender}`}>
                        <Button>
                            Respond via Email
                        </Button>
                    </a>
                </div>
            </Card.Body>
        </Card>
    )
}

export default Message
