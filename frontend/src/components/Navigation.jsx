import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';
import Firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

const Navigation = ({ user, setUser }) => {
    const Auth = Firebase.auth();

    function logout() {
        Auth.signOut()
          .then(() => {
            setUser({});
          })
          .catch(e => alert(e.message));
    }

    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand><Link to='/' className='text-dark text-decoration-none'>Real Estate Buddy</Link></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
                    <Nav>
                        <Nav.Link>
                            <Link to="/" className='text-dark text-decoration-none'>Search</Link>
                        </Nav.Link>
                        {
                            JSON.stringify(user) !== '{}' ?
                            <>
                            <Nav.Link>
                                <Link to='/post' className='text-dark text-decoration-none'>Create Listing</Link>
                            </Nav.Link>
                            <Nav.Link>
                                <Link to='/inbox' className='text-dark text-decoration-none'>Inbox</Link>
                            </Nav.Link>
                            <Nav.Link>
                                <Link to='/' className='text-dark text-decoration-none' onClick={logout}>Logout</Link>
                            </Nav.Link>
                            </> :
                            <Nav.Link>
                                <Link to='/login' className='text-dark text-decoration-none'>Sign Up or Login!</Link>
                            </Nav.Link>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default Navigation;
