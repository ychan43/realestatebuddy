import React from 'react';
import {
    InputGroup,
    FormControl,
    DropdownButton,
    Dropdown,
    Button,
    Form,
} from 'react-bootstrap';
import axios from 'axios';
import mockData from '../mock-data/Properties';

const SearchParams = ({
    locType,
    setLocType,
    location,
    setLocation,
    minPrice,
    setMinPrice,
    maxPrice,
    setMaxPrice,
    beds,
    setBeds,
    baths,
    setBaths,
    sqft,
    setSqft,
    amenities,
    setAmenities,
    setResults,
}) => {
    const locText = locType === 0 ? 'Zipcode' : locType === 1 ? 'City' : 'State';
    const findProperties = () => {
        const temp = amenities;
        for (const [key, val] of Object.entries(amenities)) {
            if (!val) {
                delete temp[key]
            }
        }
        const req = {
            city: locType === 1 ? location : '',
            state: locType === 2 ? location : '',
            zipcode: locType === 0 ? parseInt(location) : NaN,
            price_lower: minPrice === '' ? 0 : parseInt(minPrice),
            price_upper: maxPrice === '' ? Number.MAX_VALUE : parseInt(maxPrice),
            bedrooms: parseInt(beds),
            bathrooms: parseInt(baths),
            squarefeet: parseInt(sqft),
            amenities: temp,
        }
        for (const [key, val] of Object.entries(req)) {
            if (val === '' || Number.isNaN(val) || JSON.stringify(val) === '{}') {
                delete req[key]
            }
        }
        console.log(req)

        axios.get('https://thawing-plateau-85192.herokuapp.com/api/properties', { params: req }).then((res) => {
          setResults(res.data.data);
        }).catch(err => console.log(err));
    }

    return (
        <div className='mt-5 text-center'>
            <InputGroup className="mb-3">
                <FormControl
                    placeholder={`Enter Location (${locText})...`}
                    aria-label="Location Input"
                    value={location}
                    onChange={(e) => setLocation(e.currentTarget.value)}
                />
                <DropdownButton
                    variant="outline-secondary"
                    title={`Search via ${locText}`}
                    id="input-group-dropdown-2"
                    align="end"
                >
                    <Dropdown.Item onClick={()=>{
                        setLocType(0);
                        setLocation('');
                    }}>Zipcode</Dropdown.Item>
                    <Dropdown.Item onClick={()=>{
                        setLocType(1);
                        setLocation('');
                    }}>City</Dropdown.Item>
                    <Dropdown.Item onClick={()=>{
                        setLocType(2);
                        setLocation('');
                    }}>State</Dropdown.Item>
                </DropdownButton>
            </InputGroup>
            <div className='d-flex justify-content-between mb-3'>
                <div>
                    Minimum Price
                    <InputGroup className="mb-3">
                        <InputGroup.Text id="basic-addon1">$</InputGroup.Text>
                        <FormControl
                            placeholder="Enter Amount"
                            aria-label="Dollar Amount"
                            type="number"
                            min='0'
                            value={minPrice}
                            onChange={(e) => {
                                const newPrice = e.currentTarget.value;
                                setMinPrice(newPrice);
                                if (parseInt(maxPrice) < parseInt(newPrice)) {
                                    setMaxPrice(newPrice);
                                }
                            }}
                        />
                    </InputGroup>
                </div>
                <div>
                    Maximum Price
                    <InputGroup className="mb-3">
                        <InputGroup.Text id="basic-addon1">$</InputGroup.Text>
                        <FormControl
                            placeholder="Enter Amount"
                            aria-label="Dollar Amount"
                            type="number"
                            min='0'
                            value={maxPrice}
                            onChange={(e) => {
                                const newPrice = e.currentTarget.value;
                                setMaxPrice(newPrice);
                                if (parseInt(minPrice) > parseInt(newPrice)) {
                                    setMinPrice(newPrice);
                                }
                            }}
                        />
                    </InputGroup>
                </div>
                <div>
                    Beds
                    <Form.Control
                        placeholder="Number of Beds"
                        type="number"
                        value={beds}
                        min='0'
                        onChange={(e) => setBeds(e.currentTarget.value)}
                    />
                </div>
                <div>
                    Baths
                    <Form.Control
                        placeholder="Number of Baths"
                        type="number"
                        value={baths}
                        min='0'
                        onChange={(e) => setBaths(e.currentTarget.value)}
                    />
                </div>
                <div>
                    Square Feet
                    <Form.Control
                        placeholder="Square Feet"
                        type="number"
                        value={sqft}
                        min='0'
                        onChange={(e) => setSqft(e.currentTarget.value)}
                    />
                </div>
                <div>
                    Amenities
                    <DropdownButton title="Select Amenities" variant='outline-secondary'>
                    <Form>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.ac}
                                onChange={(e) => setAmenities({ ...amenities, ac: e.target.checked })}
                            />
                            <div className='m-2'>Air Conditioning</div>
                        </div>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.dishwasher}
                                onChange={(e) => setAmenities({ ...amenities, dishwasher: e.target.checked })}
                            />
                            <div className='m-2'>Dishwasher</div>
                        </div>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.laundry}
                                onChange={(e) => setAmenities({ ...amenities, laundry: e.target.checked })}
                            />
                            <div className='m-2'>In-Unit Washer/Dryer</div>
                        </div>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.wifi}
                                onChange={(e) => setAmenities({ ...amenities, wifi: e.target.checked })}
                            />
                            <div className='m-2'>Wi-Fi</div>
                        </div>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.gym}
                                onChange={(e) => setAmenities({ ...amenities, gym: e.target.checked })}
                            />
                            <div className='m-2'>Gym</div>
                        </div>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.pool}
                                onChange={(e) => setAmenities({ ...amenities, pool: e.target.checked })}
                            />
                            <div className='m-2'>Pool</div>
                        </div>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.balcony}
                                onChange={(e) => setAmenities({ ...amenities, balcony: e.target.checked })}
                            />
                            <div className='m-2'>Balcony</div>
                        </div>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.pet}
                                onChange={(e) => setAmenities({ ...amenities, pet: e.target.checked })}
                            />
                            <div className='m-2'>Pet Friendly</div>
                        </div>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.parking}
                                onChange={(e) => setAmenities({ ...amenities, parking: e.target.checked })}
                            />
                            <div className='m-2'>Parking Available</div>
                        </div>
                        <div className='d-flex'>
                            <Form.Check
                                className='m-2'
                                type={'checkbox'}
                                checked={amenities.furnished}
                                onChange={(e) => setAmenities({ ...amenities, furnished: e.target.checked })}
                            />
                            <div className='m-2'>Furnished</div>
                        </div>
                    </Form>
                    </DropdownButton>
                </div>
            </div>
            <Button variant="primary" size="lg" onClick={findProperties}>
                Search!
            </Button>
        </div>
    )
}

export default SearchParams
