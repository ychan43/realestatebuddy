import React from 'react';
import { Card, Button, Badge } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Listing = ({ data }) => {
    const ammenitiesMap = {
        pet: 'Pet Friendly',
        wifi: 'Wi-Fi',
        ac: 'Air Conditioning',
        laundry: 'In-Unit Washer/Dryer',
        dishwasher: 'Dishwasher',
        furnished: 'Furnished',
        parking: 'Parking Available',
        balcony: 'Balcony',
        gym: 'Gym',
        pool: 'Pool',
    }

    return (
        <Card>
            <Card.Body>
                <div className="d-flex justify-content-between">
                    <div>
                        <Card.Title>{data.name}</Card.Title>
                        <Card.Subtitle className="mb-2 text-muted">
                            {data.address} {data.apt_number}, {data.city}, {data.state} | Beds: {data.bedrooms} | Baths: {data.bathrooms} | Square Feet: {data.squarefeet} | Zip Code: {data.zipcode}
                        </Card.Subtitle>
                    </div>
                    <div>
                        <Card.Title>${data.price}</Card.Title>
                    </div>
                </div>
                <Card.Text>
                    {data.description?.substring?.(0, 500)}
                </Card.Text>
                <div className="d-flex justify-content-between">
                    <Card.Subtitle className="mb-2 text-muted">
                        {data.amenities && Object.entries(data.amenities).map(([key, val]) =>
                            val && <><Badge bg="secondary">{ammenitiesMap[key]}</Badge>&nbsp;</>
                        )}
                    </Card.Subtitle>
                    <Link to={`/details/${data._id}`}>
                        <Button>
                            See Listing!
                        </Button>
                    </Link>
                </div>
            </Card.Body>
        </Card>
    );
};

export default Listing;
