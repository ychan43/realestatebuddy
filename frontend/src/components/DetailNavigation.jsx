import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import MessageModal from './MessageModal';

const DetailNavigation = ({ user, data }) => {
    const [show, setShow] = useState(false);
    const history = useHistory();

    return (
        <div className='d-flex justify-content-between'>
            <Button
                variant='danger'
                onClick={history.goBack}
            >
                Back
            </Button>
            <Button
                variant={JSON.stringify(user) !== '{}' ? 'primary' : 'secondary'}
                onClick={JSON.stringify(user) !== '{}' ? () => setShow(true)
                : () => alert('Sign in to send a message!')}
            >
                Send a Message!
            </Button>
            <MessageModal
                show={show}
                data={data}
                onHide={() => setShow(false)}
                user={user}
            />
        </div>
    )
}

export default DetailNavigation
