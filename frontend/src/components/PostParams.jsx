import React from 'react';
import {
    Form,
    InputGroup,
    FormControl,
    DropdownButton,
    Row,
    Col,
    Button,
} from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

const PostParams = ({
    name,
    setName,
    address,
    setAddress,
    aptNumber,
    setAptNumber,
    city,
    setCity,
    state,
    setState,
    zipcode,
    setZipcode,
    sqft,
    setSqft,
    price,
    setPrice,
    beds,
    setBeds,
    baths,
    setBaths,
    amenities,
    setAmenities,
    desc,
    setDesc,
    user,
}) => {
    const history = useHistory();
    const postProperty = () => {
        const req = {
            name,
            address,
            //apt_number: aptNumber,
            city,
            state,
            zipcode: parseInt(zipcode),
            squarefeet: parseInt(sqft),
            price: parseInt(price),
            bedrooms: parseInt(beds),
            bathrooms: parseInt(baths),
            amenities,
            description: desc,
        };
        if (!Object.values(req).every((val) => val !== '' && !Number.isNaN(val))) {
            alert('Please fill the entire form properly.')
            return;
        }
        req.apt_number = aptNumber;
        req.email = user.email;
        console.log(req);

        axios.post('https://thawing-plateau-85192.herokuapp.com/api/properties',
          req
        ).catch(err => (console.log(err)));

        history.push('/');
    }

    return (
        <>
        <Row className='text-center mt-5'>
            <Col>
                Property Name
                <Form.Control
                    placeholder="Property Name"
                    value={name}
                    min='0'
                    onChange={(e) => setName(e.currentTarget.value)}
                />
            </Col>
            <Col>
                Address
                <Form.Control
                    placeholder="Address"
                    value={address}
                    min='0'
                    onChange={(e) => setAddress(e.currentTarget.value)}
                />
            </Col>
            <Col>
                Apartment/Unit Number
                <Form.Control
                    placeholder="Optional"
                    value={aptNumber}
                    min='0'
                    onChange={(e) => setAptNumber(e.currentTarget.value)}
                />
            </Col>
        </Row>
        <Row className='text-center mt-3'>
            <Col>
                City
                <Form.Control
                    placeholder="City"
                    value={city}
                    min='0'
                    onChange={(e) => setCity(e.currentTarget.value)}
                />
            </Col>
            <Col>
                State
                <Form.Control
                    placeholder="State"
                    value={state}
                    min='0'
                    onChange={(e) => setState(e.currentTarget.value)}
                />
            </Col>
            <Col>
                Zipcode
                <Form.Control
                    placeholder="Zipcode"
                    value={zipcode}
                    min='0'
                    onChange={(e) => setZipcode(e.currentTarget.value)}
                />
            </Col>
        </Row>
        <div className='d-flex justify-content-between mt-3 text-center'>
            <div>
                Price
                <InputGroup className="mb-3">
                    <InputGroup.Text id="basic-addon1">$</InputGroup.Text>
                    <FormControl
                        placeholder="Price (USD)"
                        aria-label="Dollar Amount"
                        type="number"
                        min='0'
                        value={price}
                        onChange={(e) => setPrice(e.currentTarget.value)}
                    />
                </InputGroup>
            </div>
            <div>
                Beds
                <Form.Control
                    placeholder="Number of Beds"
                    type="number"
                    value={beds}
                    min='0'
                    onChange={(e) => setBeds(e.currentTarget.value)}
                />
            </div>
            <div>
                Baths
                <Form.Control
                    placeholder="Number of Baths"
                    type="number"
                    value={baths}
                    min='0'
                    onChange={(e) => setBaths(e.currentTarget.value)}
                />
            </div>
            <div>
                Square Feet
                <Form.Control
                    placeholder="Square Feet"
                    type="number"
                    value={sqft}
                    min='0'
                    onChange={(e) => setSqft(e.currentTarget.value)}
                />
            </div>
            <div>
                Amenities
                <DropdownButton title="Select Amenities" variant='outline-secondary'>
                <Form>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.ac}
                            onChange={(e) => setAmenities({ ...amenities, ac: e.target.checked })}
                        />
                        <div className='m-2'>Air Conditioning</div>
                    </div>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.dishwasher}
                            onChange={(e) => setAmenities({ ...amenities, dishwasher: e.target.checked })}
                        />
                        <div className='m-2'>Dishwasher</div>
                    </div>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.laundry}
                            onChange={(e) => setAmenities({ ...amenities, laundry: e.target.checked })}
                        />
                        <div className='m-2'>In-Unit Washer/Dryer</div>
                    </div>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.wifi}
                            onChange={(e) => setAmenities({ ...amenities, wifi: e.target.checked })}
                        />
                        <div className='m-2'>Wi-Fi</div>
                    </div>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.gym}
                            onChange={(e) => setAmenities({ ...amenities, gym: e.target.checked })}
                        />
                        <div className='m-2'>Gym</div>
                    </div>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.pool}
                            onChange={(e) => setAmenities({ ...amenities, pool: e.target.checked })}
                        />
                        <div className='m-2'>Pool</div>
                    </div>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.balcony}
                            onChange={(e) => setAmenities({ ...amenities, balcony: e.target.checked })}
                        />
                        <div className='m-2'>Balcony</div>
                    </div>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.pet}
                            onChange={(e) => setAmenities({ ...amenities, pet: e.target.checked })}
                        />
                        <div className='m-2'>Pet Friendly</div>
                    </div>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.parking}
                            onChange={(e) => setAmenities({ ...amenities, parking: e.target.checked })}
                        />
                        <div className='m-2'>Parking Available</div>
                    </div>
                    <div className='d-flex'>
                        <Form.Check
                            className='m-2'
                            type={'checkbox'}
                            checked={amenities.parking}
                            onChange={(e) => setAmenities({ ...amenities, parking: e.target.checked })}
                        />
                        <div className='m-2'>Parking Available</div>
                    </div>
                </Form>
                </DropdownButton>
            </div>
        </div>
        <Form.Group className="mb-5" controlId="exampleForm.ControlTextarea1">
            <Form.Label>Description of Property</Form.Label>
            <Form.Control
                as="textarea"
                rows={5}
                value={desc}
                onChange={(e) => setDesc(e.currentTarget.value)}
            />
        </Form.Group>
        <div className="text-center">
            <Button variant="primary" size="lg" onClick={postProperty}>
                Create Listing!
            </Button>
        </div>
        </>
    )
}

export default PostParams;
