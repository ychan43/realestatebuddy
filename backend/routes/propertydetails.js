var Property = require('../models/property');

module.exports = function (router) {

  var propertyDetailsRoute = router.route('/properties/:id');

  propertyDetailsRoute.get(async function (req, res) {
    try {
      const property = await Property.findById(req.params.id)
      
      if (property == null) {
        res.status(404).json({
          message: "No property with the ID found",
          data: []
        })
        return;
      }
      
      res.status(200).json({
        message: "OK",
        data: property
      })
    }
    catch {
      // Return internal server error
      res.status(500).json({ 
        message: 'Error 500: Internal Server Error',
        data: []
      })
    }
    
  });

  propertyDetailsRoute.put(async function (req, res) {
    try {
      // Find property with given ID
      const property = await Property.findById(req.params.id);
      // Check if the property doesn't exist with the ID
      if (property === null) {
        res.status(404).json({
          message: "No property with ID found",
          data: []
        })
      }
      // Update the property
      else {
        property.name = req.body.name;
        property.address = req.body.address;
        property.apt_number = req.body.apt_number;
        property.city = req.body.city;
        property.state = req.body.state;
        property.zipcode = req.body.zipcode;
        property.price = req.body.price;
        property.bedrooms = req.body.bedrooms;
        property.bathrooms = req.body.bathrooms;
        property.squarefeet = req.body.squarefeet;
        property.amenities = req.body.amenities;
        property.description = req.body.description;
        property.creator = req.body.creator;

        property.save(function (error) {
          if (error) {
            if (req.body.name == undefined) {
              res.status(400).json( {message: "ERROR: `name` is required", data: []} );
            } else if (req.body.address == undefined) {
              res.status(400).json( {message: "ERROR: `address` is required", data: []} );
            } else if (req.body.city == undefined) {
              res.status(400).json( {message: "ERROR: `city` is required", data: []} );
            } else if (req.body.state == undefined) {
              res.status(400).json( {message: "ERROR: `state` is required", data: []} );
            } else if (req.body.zipcode == undefined) {
              res.status(400).json( {message: "ERROR: `zipcode` is required", data: []} );
            } else if (req.body.price == undefined) {
              res.status(400).json( {message: "ERROR: `price` is required", data: []} );
            } else if (req.body.bedrooms == undefined) {
              res.status(400).json( {message: "ERROR: `bedrooms` is required", data: []} );
            } else if (req.body.bathrooms == undefined) {
              res.status(400).json( {message: "ERROR: `bathrooms` is required", data: []} );
            } else if (req.body.squarefeet == undefined) {
              res.status(400).json( {message: "ERROR: `squarefeet` is required", data: []} );
            } else if (req.body.creator == undefined) {
              res.status(400).json( {message: "ERROR: `creator` is required", data: []} );
            } else {
              res.status(400).json( {message: "ERROR: Invalid input", data: []} );
            }
          } else {
            res.status(200).json({
              message: "OK",
              data: property
            })
          }
        })
      }
    }
    catch {
      // Return internal server error
      res.status(500).json({ 
        message: 'Error 500: Internal Server Error',
        data: []
      })
    }
  });

  propertyDetailsRoute.delete(async function (req, res) {
    try {
      // get the user by id
      const user = await User.findById(req.params.id);
      // check if the user could be found
      if (user == null) {
          res.status(404).json({
            message:'Error 404: No property with the ID to delete',
            data: []
          });
          return;
      } else {
        await Property.deleteOne({ _id: req.params.id })
        res.status(200).json({
          message: "OK",
          data: []
        })
        return;
      }
    }
    catch {
      // Return internal server error
      res.status(500).json({ 
        message: 'Error 500: Internal Server Error',
        data: []
      })
      return;
    }
  });

  return router;
}
