module.exports = function (app, router) {
    app.use('/api', require('./home.js')(router));
    app.use('/api/properties', require('./properties.js')(router));
    app.use('/api/properties/:id', require('./propertydetails.js')(router));
    app.use('/api/messages', require('./messages.js')(router));
    app.use('/api/messages/:id', require('./messagedetails.js')(router));
    app.use('/api/users', require('./users.js')(router));
    // app.use('/api/users/:id', require('./userdetails.js')(router));
};
