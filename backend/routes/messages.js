var Message = require('../models/message');
var User = require('../models/user');

module.exports = function (router) {

  var messagesRoute = router.route('/messages');

  messagesRoute.get(async function (req, res) {  // unique sender, recipient, property ids
    try {
      var where = req.query.where;
      if (req.query.where) {
        where = JSON.parse(req.query.where);
      }

      const message = await Message.find(where);
      res.status(200).json({
          message: "OK",
          data: message   // return messages where given email is recipient, property info, date created
      });
    } catch (error) {
        res.status(500).json({
          message: "Server error",
          data: []
        })
    }
  });

  messagesRoute.post(async function (req, res)
  {
    var newMessage = new Message();
    newMessage.sender = req.body.sender;
    newMessage.recipient = req.body.recipient;
    newMessage.property_id = req.body.property_id;
    newMessage.property_address = req.body.property_address;
    newMessage.apt_number = req.body.apt_number;
    newMessage.message = req.body.message;

    const user_sender = await User.findOne( {email: req.body.sender}, {_id: 0} );
    if (user_sender == null) {
      res.status(404).json({
        message: "ERROR: No user found with given email",
        data: []
      });
      return;
    }

    const user_recipient = await User.findOne( {email: req.body.recipient}, {_id: 0} );

    if (user_recipient == null) {
      res.status(404).json({
        message: "ERROR: No user found with given email",
        data: []
      });
      return;
    }

    newMessage.save(function (error) {
      if (error) {
        if (req.body.sender == undefined) res.status(400).json( {message: "ERROR: `sender` is required", data: []} );
        else if (req.body.recipient == undefined) res.status(400).json( {message: "ERROR: `recipient` is required", data: []} );
        else if (req.body.property_id == undefined) res.status(400).json( {message: "ERROR: `property id` is required", data: []} );
        else if (req.body.property_address == undefined) res.status(400).json( {message: "ERROR: `property address` is required", data: []} );
        else if (req.body.apt_number == undefined) res.status(400).json( {message: "ERROR: `apt number` is required", data: []} );
      } else res.status(201).json({
        message: "OK",
        data: newMessage
      });
    });
  });

  return router;
}
