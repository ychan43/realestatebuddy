var Message = require('../models/message');

module.exports = function (router) {

  var messageDetailsRoute = router.route('/messages/:id');

  messageDetailsRoute.get(async function (req, res) {
    try {
      const data = await Message.findById({ _id: req.params.id })
      if (data == null) {
        res.status(404).json({ 
            message: "ERROR: Message does not exist",
            data: []
        })
        return;
      }

      res.status(200).json({
          message: "OK",
          data: data
      })
    }
    catch {
      // Return internal server error
      res.status(500).json({ 
        message: 'Error 500: Internal Server Error',
        data: []
      })
      return;
    }
  });

  messageDetailsRoute.put(function (req, res) {


  });

  messageDetailsRoute.delete(async function (req, res) {
    try {
      let data = await Message.findOne({ _id: req.params.id })
      if (data == null) {
        res.status(404).json({ 
          message: "ERROR: Message does not exist.",
          data: []
        })
        return;
      }

      await Message.deleteOne({ _id: req.params.id })
      res.status(200).json({
          message: "OK",
          data: []
      })
    } catch {
      // Return internal server error
      res.status(500).json({ 
        message: 'Error 500: Internal Server Error',
        data: []
      })
      return;
    }

  });

  return router;
}
