var User = require('../models/user');

module.exports = function (router) {

  var userRoute = router.route('/users');

  userRoute.get(async function (req, res) {
    try {
      if (req.params.email === undefined) {
        res.status(400).json({
          message: "`email` is required",
          data: []
        })
        return;
      }

      var user = await User.findOne({ email: req.params.email });

      if (user == null) {
        res.status(404).json({
          message: "No user found with given email",
          data: []
        })
      } else {
        res.status(200).json({
          message: "OK",
          data: user
        })
      }
    } catch {
      res.status(500).json({
        message: "Server error",
        data: []
      })
    }
  });

  userRoute.post(async function (req, res) {
    // check required fields are given
    if (req.body.email === undefined) {
      res.status(400).json( {message: "ERROR: `email` is required", data: []} );
      return;
    }

    // check if new user has a unique email (i.e. email doesn't already exist with a user)
    const users = await User.find( { email: req.body.email } );
    if (users.length != 0) {
      res.status(400).json( {message: "ERROR: Multiple users cannot have the same email", data: []} );
      return;
    }

    var newUser = new User();
    newUser.email = req.body.email;

    newUser.save(function (error) {
      if (error) {
        res.status(500).json({
          message: "Server error",
          data: []
        })
      } else {
        res.status(201).json({
          message: "OK",
          data: newUser
        })
      }
    })
  });

  return router;
}
