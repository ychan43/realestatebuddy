var Property = require('../models/property');
var User = require('../models/user');

module.exports = function (router) {

  var propertiesRoute = router.route('/properties');

  //Required to have price_lower and price_upper set when using this get
  propertiesRoute.get(async function (req, res) {
    // get a copy and filter by price range
    let properties_json = req.query;
    properties_json.price = {$gte: req.query.price_lower, $lte: req.query.price_upper};

    if (req.query.squarefeet) {
      properties_json.squarefeet = {$gte: req.query.squarefeet};
    }

    var pet;
    var wifi;
    var ac;
    var laundry;
    var dishwasher;
    var furnished;
    var parking;
    var balcony;
    var gym;
    var pool;

    if (req.query.amenities) {
      // console.log(req.query.amenities);
      const obj = JSON.parse(req.query.amenities);

      for (var key in obj) {
        if (key === "pet") {
          pet = {"amenities.pet": true};
        } else if (key === "wifi") {
          wifi = {"amenities.wifi": true};
        } else if (key === "ac") {
          ac = {"amenities.ac": true};
        } else if (key === "laundry") {
          laundry = {"amenities.laundry": true};
        } else if (key === "dishwasher") {
          dishwasher = {"amenities.dishwasher": true};
        } else if (key === "furnished") {
          furnished = {"amenities.furnished": true};
        } else if (key === "parking") {
          parking = {"amenities.parking": true};
        } else if (key === "balcony") {
          balcony = {"amenities.balcony": true};
        } else if (key === "gym") {
          gym = {"amenities.gym": true};
        } else if (key === "pool") {
          pool = {"amenities.pool": true};
        }
      }

      delete properties_json.amenities;
    }

    // remove the following fields from the filter JSON
    delete properties_json.price_lower;
    delete properties_json.price_upper;

    try {
      const properties = await Property.find(pet).find(wifi).find(ac).find(laundry).find(dishwasher).find(furnished).find(parking).find(balcony).find(gym).find(pool).find(properties_json);

      res.status(200).json({
          message: "OK",
          data: properties
      });
    } catch (error) {
        res.status(500).json({
          message: "Server error",
          data: []
        })
    }
  });

  propertiesRoute.post(function (req, res) {
    var newProperty = new Property();
    newProperty.name = req.body.name;
    newProperty.address = req.body.address;
    newProperty.apt_number = req.body.apt_number;
    newProperty.city = req.body.city;
    newProperty.state = req.body.state;
    newProperty.zipcode = req.body.zipcode;
    newProperty.price = req.body.price;
    newProperty.bedrooms = req.body.bedrooms;
    newProperty.bathrooms = req.body.bathrooms;
    newProperty.squarefeet = req.body.squarefeet;
    newProperty.amenities = req.body.amenities;
    newProperty.description = req.body.description;
    newProperty.creator = req.body.email;

    newProperty.save(function (error) {
      if (error) {
        if (req.body.name == undefined) {
          res.status(400).json( {message: "ERROR", data: "`name` is required"} );
        } else if (req.body.address == undefined) {
          res.status(400).json( {message: "ERROR", data: "`address` is required"} );
        } else if (req.body.city == undefined) {
          res.status(400).json( {message: "ERROR", data: "`city` is required"} );
        } else if (req.body.state == undefined) {
          res.status(400).json( {message: "ERROR", data: "`state` is required"} );
        } else if (req.body.zipcode == undefined) {
          res.status(400).json( {message: "ERROR", data: "`zipcode` is required"} );
        } else if (req.body.price == undefined) {
          res.status(400).json( {message: "ERROR", data: "`price` is required"} );
        } else if (req.body.bedrooms == undefined) {
          res.status(400).json( {message: "ERROR", data: "`bedrooms` is required"} );
        } else if (req.body.bathrooms == undefined) {
          res.status(400).json( {message: "ERROR", data: "`bathrooms` is required"} );
        } else if (req.body.squarefeet == undefined) {
          res.status(400).json( {message: "ERROR", data: "`squarefeet` is required"} );
        } else if (req.body.email == undefined) {
          res.status(400).json( {message: "ERROR", data: "`creator` is required"} );
        } else {
          res.status(400).json( {message: "ERROR", data: "Invalid input"} );
        }
      } else {
        res.status(201).json({
          message: "OK",
          data: newProperty
        })
      }
    });
  });

  return router;
}
