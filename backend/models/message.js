var mongoose = require('mongoose');

var MessageSchema = new mongoose.Schema({
  dateCreated: { type: Date, default: Date.now },
  sender: { type: String, required: true },
  sender_name: { type: String },
  recipient: { type: String, required: true },
  recipient_name: { type: String },
  property_id: { type: String, required: true },
  property_address: { type: String, required: true },
  apt_number: { type: String },
  message: { type: String, required: true },
});

module.exports = mongoose.model('Message', MessageSchema);
