var mongoose = require('mongoose');

var PropertySchema = new mongoose.Schema({
  name: { type: String, required: true },
  address: { type: String, required: true },
  apt_number: String,
  city: { type: String, required: true },
  state: { type: String, required: true },
  zipcode: { type: Number, required: true },
  price: { type: Number, required: true },
  bedrooms: { type: Number, required: true },
  bathrooms: { type: Number, required: true },
  squarefeet: { type: Number, required: true },
  amenities: {
    pet: false,
    wifi: false,
    ac: false,
    laundry: false,
    dishwasher: false,
    furnished: false,
    parking: false,
    balcony: false,
    gym: false,
    pool: false
  },
  description: String,
  creator: { type: String, required: true }
});

module.exports = mongoose.model('Property', PropertySchema);
